{ sources, buildGoModule, lib, stdenv }:
buildGoModule rec {
  pname = "ory-hydra";
  inherit (sources.ory-hydra) version src;

  vendorSha256 = "sha256-xGe50VHnjyCJ3XGJ95S+Axngcp/Jmkn/V65iDYVmE+c=";

  subPackages = [ "." ];

  tags = [ "sqlite" ];

  doCheck = false;

  preBuild = ''
    # Patch shebangs
    files=(
       test/e2e/run.sh
       script/testenv.sh
       script/test-envs.sh
       script/debug-entrypoint.sh
    )
    patchShebangs "''${files[@]}"

    # patchShebangs doesn't work for this Makefile, do it manually
    substituteInPlace Makefile --replace '/bin/bash' '${stdenv.shell}'
  '';

  meta = with lib; {
    homepage = "https://www.ory.sh/hydra/";
    license = licenses.asl20;
    description = "OAuth 2.0 server with OpenID Connect support";
  };
}
