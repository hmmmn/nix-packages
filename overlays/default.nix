let repoPkgs = nixpkgs: (import ../pkgs { pkgs = nixpkgs; lib = nixpkgs.lib; }); in
{
  ory-hydra = final: prev: {
    ory-hydra = (repoPkgs prev).ory-hydra;
  };
  hydroxide = import ./hydroxide;
}
