{
  description = "hmmmn's repository cause flake's not a lake";

  inputs.flake-utils.url = "github:numtide/flake-utils";
  inputs.nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";

  outputs = { nixpkgs, flake-utils, ... }:
    flake-utils.lib.eachDefaultSystem
      (system:
        let
          pkgs = import nixpkgs {
            inherit system;
            overlays = builtins.attrValues (import ./overlays);
          };
          repo = import ./. { inherit pkgs; };
        in
        {
          packages = {
            inherit (repo) ory-hydra;
            inherit (pkgs) hydroxide;
          };
          devShell = pkgs.mkShell {
            name = "hmmmn-nix-packages-dev-shell";
            buildInputs = with pkgs; [
              nix
              nvfetcher
            ];
          };
        }) // {
      overlays = import ./overlays;
    };

}
