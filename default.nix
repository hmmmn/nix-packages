{ pkgs ? import <nixpkgs> { } }:
import ./pkgs/default.nix { inherit pkgs; lib = pkgs.lib; } // {
  overlays = import ./overlays;
}
