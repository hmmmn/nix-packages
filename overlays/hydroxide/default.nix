final: prev:
let
  sources = prev.callPackage (import ../../_sources/generated.nix) { };
in
{
  hydroxide = prev.hydroxide.overrideAttrs (old: {
    inherit (sources.hydroxide) version src;
    patches = [
      ./hydroxide-fix-login-user-agent.patch
    ];
  });
}
